// vue.config.js

/**
 * @type {import('@vue/cli-service').ProjectOptions}
 */
module.exports = {
  // options...
  lintOnSave: process.env.NODE_ENV !== 'production',
};
