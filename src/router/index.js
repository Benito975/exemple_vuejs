import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '@/views/Home.vue';
import Details from '@/views/Details.vue';
import Login from '@/views/Login.vue';
import SignUp from '@/views/SignUp.vue';
import Profile from '@/views/Profile.vue';
import About from '@/views/About.vue';
import Favorites from '@/views/Favorites.vue';
import Episodes from '@/views/Episodes.vue';
import DetailsEpisode from '@/views/DetailsEpisode.vue';
import ViewEpisode from '@/views/ViewEpisode.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/details/:TvShowId',
    name: 'Details',
    component: Details,
    props: true,
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
  },
  {
    path: '/signup',
    name: 'SignUp',
    component: SignUp,
  },
  {
    path: '/profile',
    name: 'Profile',
    component: Profile,
  },
  {
    path: '/about',
    name: 'About',
    component: About,
  },
  {
    path: '/favorites',
    name: 'Favorites',
    component: Favorites,
  },
  {
    path: '/episodes/:SeasonId',
    name: 'Episodes',
    component: Episodes,
    props: true,
  },
  {
    path: '/detailsepisode/:EpisodeId',
    name: 'DetailsEpisode',
    component: DetailsEpisode,
    props: true,
  },
  {
    path: '/viewepisode/:EpisodeId',
    name: 'ViewEpisode',
    component: ViewEpisode,
    props: true,
  },
];

const router = new VueRouter({
  routes,
});

export default router;
