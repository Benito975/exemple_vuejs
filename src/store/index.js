import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    token: '',
  },
  getters: {
    isLoggedIn: (state) => !!state.token,
  },
  mutations: {
    setToken(state, newToken) {
      state.token = newToken;
    },
    logout(state) {
      state.token = '';
    },
  },
  actions: {
    storeToken(context, newToken) {
      context.commit('setToken', newToken);
    },

    storeLogout(context) {
      context.commit('logout');
    },
  },
  modules: {
  },
});
