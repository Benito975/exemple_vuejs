import Vue from 'vue';
import persistentState from 'vue-persistent-state';
import App from './App.vue';
import router from './router';
import store from './store';

Vue.config.productionTip = false;

const initialState = {
  srvUrl: '',
};

Vue.use(persistentState, initialState);

new Vue({
  router,
  store,
  render(h) { return h(App); },
}).$mount('#app');
